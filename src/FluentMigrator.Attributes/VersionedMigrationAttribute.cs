#region License

// 
// Copyright (c) 2015, Sławomir Pawluk
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//   http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#endregion

namespace FluentMigrator.Annotations
{
  /// <summary>
  /// MigrationAttribute that enforce version number in format yyyyMMddN where 
  /// yyyy - year in format 2015
  /// MM - month in format 12
  /// dd - day in format 01
  /// N - numer of migration
  /// </summary>
  public class VersionedMigrationAttribute : MigrationAttribute
  {
    public string Author { get; private set; }

    public VersionedMigrationAttribute(string author, int year, int month, int day, int number)
      : base(CalculateMigrationVersion(year, month, day, number))
    {
      Author = author;
    }

    private static long CalculateMigrationVersion(int year, int month, int day, int number)
    {
      return year * 1000000L + month * 10000L + day * 100L + number;
    }
  }
}